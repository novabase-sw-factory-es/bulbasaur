# bulbasaur

Demonstrator of Jekyll with CI to GitLab-CI and CD to Netlify.

* Install Visual Studio Code with "Remote - Containers" plugin
* Install Docker Desktop for Windows
* Open Visual Studio Code and clone project
* Open the project folder
* When offered to reopen in container, do so (alternatively, open in container yourself)
* _tools/initDependencies.sh (only needed once)
* _tools/serve.sh will serve the compiled project in http://localhost:4000 and will automatically recompile as needed

Continuosly Deployed to:

* Preproduction: https://pre-bulbasaur-neotalent.netlify.com [![Netlify Status](https://api.netlify.com/api/v1/badges/4705f5ea-30d1-45d8-b98b-1042a3954d21/deploy-status)](https://app.netlify.com/sites/pre-bulbasaur-neotalent/deploys)

* Production: https://bulbasaur-neotalent.netlify.com [![Netlify Status](https://api.netlify.com/api/v1/badges/7d55c17f-0f33-43a6-bba6-c9c3f6f843c5/deploy-status)](https://app.netlify.com/sites/bulbasaus-neotalent/deploys) (automatically deployed but manually triggered).

Content may be managed directly in this source code or in the [Content manager](https://dev-bulbasaur-neotalent.netlify.com/admin/) and previewed in the [Development environment](https://dev-bulbasaur-neotalent.netlify.com/). Deployments are managed at [Netlify](https://app.netlify.com/teams/cochedorado/sites).
