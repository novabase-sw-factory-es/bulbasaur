---
title: About
layout: page
permalink: /about/
---
(C) [Novabase Neotalent](https://www.novabase.pt/en/dp/neotalent) (part of [Novabase Group](https://www.novabase.pt/en)) 2019

Content is managed in the [Content manager](https://dev-bulbasaur-neotalent.netlify.com/admin/) and previewed in the [Development environment](https://dev-bulbasaur-neotalent.netlify.com/). [![Netlify Status](https://api.netlify.com/api/v1/badges/f3abbbde-4c74-4c64-9f4c-2f777c724852/deploy-status)](https://app.netlify.com/sites/dev-bulbasaur-neotalent/deploys)

This site is built and Continuously Deployed to:

* [Preproduction at Netlify](https://pre-bulbasaur-neotalent.netlify.com). 
  [![pipeline status](https://gitlab.com/novabase-sw-factory-es/bulbasaur/badges/master/pipeline.svg)](https://gitlab.com/novabase-sw-factory-es/bulbasaur/pipelines) [![Netlify Status](https://api.netlify.com/api/v1/badges/4705f5ea-30d1-45d8-b98b-1042a3954d21/deploy-status)](https://app.netlify.com/sites/pre-bulbasaur-neotalent/deploys)
* [Production at Netlify](https://bulbasaur-neotalent.netlify.com). [![pipeline status](https://gitlab.com/novabase-sw-factory-es/bulbasaur/badges/master/pipeline.svg)](https://gitlab.com/novabase-sw-factory-es/bulbasaur/pipelines) [![Netlify Status](https://api.netlify.com/api/v1/badges/7d55c17f-0f33-43a6-bba6-c9c3f6f843c5/deploy-status)](https://app.netlify.com/sites/bulbasaus-neotalent/deploys) (automatically deployed but manually triggered).

Source code can be found at [GitLab](https://gitlab.com/novabase-sw-factory-es/bulbasaur).

Deployments are managed at [Netlify](https://app.netlify.com/teams/cochedorado/sites).

T2