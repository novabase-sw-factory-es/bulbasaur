---
layout: post
title: Updated description
date: '2019-11-30 12:01:08 +0000'
excerpt_separator: <!--more-->
---
The description for the page has been updated.

The [description menu entry](/description/) of this page includes... well... exactly that: a description of the technologies used here. Some screenshot were missing so we added them. Enjoy it!

<!--more-->
