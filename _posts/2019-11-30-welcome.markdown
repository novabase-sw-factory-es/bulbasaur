---
layout: post
title: Welcome
date: '2019-11-30 09:00:00 +0000'
excerpt_separator: <!--more-->
---
Welcome to this demonstrator page. This page uses Jekyll as static CMS. A full description can be found in the [menu](/description/).

In the meanwhile, just contact us at javier.sedano@neotalent.es if you need anything...
