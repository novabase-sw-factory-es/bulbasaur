---
title: Description
layout: page
permalink: /description/
---
This page uses [Jekyll](https://jekyllrb.com/) technology. Jekyll is a static CMS: the content is defined through files in a kind of "source code" that is "compiled" to the final page and deployed. This approach provides almost the full flexibilty of a dynamic CMS with a few benefits:

* It is not subject to security problems, because... well... all the content is static.
* All the content is simply plain files, so they can be easily stored in your favourite Version Control System. In this case, we are using [GitLab](https://gitlab.com/novabase-sw-factory-es/bulbasaur).
* The page is "compiled", so the usual technics used by developers to guarantee the quality of the result can be used here: Continuous Integration and Continuous Delivery, branch-based modifications, canary deployments,... In this case, we are using [GitLab-CI](https://docs.gitlab.com/ee/ci/), which is a complete suite of CI tools.
* Production deployment can be automated. In this case we are deploying to [Netlify](https://bulbasaur-neotalent.netlify.com), but for example [Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine/) or any other Kubernetes provider is doable. The deployment is automatic, but the trigger is manual.
* Preproduction deployment can be automated. Since Preproduction and Production must be as much as similar, it is also done to [Netlify](https://pre-bulbasaur-neotalent.netlify.com), but in difference with Production it is triggered everytime new content is uploaded to GitLab (to master branch).

However, it comes with a drawback: the generation of content is slightly harder than with a dynamic CMS such as Wordpress or Joomla. To mitigate this problem, we have made the following:

* Contents can be managed in a [Netlify environment](https://dev-bulbasaur-neotalent.netlify.com/admin/) (authentication required) and previewed [next to it](https://dev-bulbasaur-neotalent.netlify.com/). The content editor is not as much as powerful as the full-fledged environment described below, but is much easier to use and only needs a browser. The following screenshoots show how easy it is and how the preview helps avoiding mistakes.

![markdown](/assets/resources/netlify-admin-overview.png)

![markdown](/assets/resources/netlify-admin-edit.png)

* The full-fledged environment can be run in a computer needing only [Docker Desktop for Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows) and [Visual Studio Code](https://code.visualstudio.com/), following the instructions on the README in the source code.

* Contents can be written in [HTML](https://www.w3schools.com/html/) or [MarkDown](https://en.wikipedia.org/wiki/Markdown). For example this page has been written in MarkDown. The following is a screenshoot of this post while was being written, showing how Visual Studio Code shows the result while writting it:

![markdown](/assets/resources/markdown.png)

